import path from "path";
import HtmlWebpackPlugin from "html-webpack-plugin";
import { CleanWebpackPlugin } from "clean-webpack-plugin";
import CopyPlugin from "copy-webpack-plugin";

module.exports = {
  mode: "production",
  entry: {
    main: [path.resolve(__dirname, "./src/index.js")]
  },
  output: {
    path: path.resolve(__dirname, "./dist"),
    filename: "assets/js/[name].[contenthash].js",
    publicPath: "/"
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
    ]
  },
  plugins: [
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, "./src/index.html"),
      filename: path.resolve(__dirname, "./dist/views/index.pug"),
      minify: {
        quoteCharacter: "'",
        collapseWhitespace: true
      }
    }),
    new CopyPlugin(["./package.json"])
  ]
};
