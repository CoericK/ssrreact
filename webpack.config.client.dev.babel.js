import path from "path";
import webpack from "webpack";
import HtmlWebpackPlugin from "html-webpack-plugin";
import { CleanWebpackPlugin } from "clean-webpack-plugin";

const client = {
  mode: "development",
  context: path.resolve(__dirname, "./src"),
  entry: {
    main: [
      "react-hot-loader/patch",
      "webpack-hot-middleware/client?reload=true",
      path.resolve(__dirname, "./src/index.js")
    ]
  },
  output: {
    path: path.resolve(__dirname, "./dist"),
    filename: "assets/js/[name].js",
    publicPath: "/"
  },
  resolve: {
    extensions: [".js", ".jsx"],
    alias: {
      "react-dom": "@hot-loader/react-dom"
    }
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      }
    ]
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin()
    /*
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, "./src/index.html")
    })*/
  ]
};

module.exports = client;
