import axios from 'axios'



const headers = new Headers()
console.log('headers', headers)

const getProducts = async () => {
  let response
  try {
    response = await axios.get('http://localhost:3000/api/products')
  } catch (error) {
    console.log('error', error)
  }
  return response.data
}

const getProductDetail = async (id) => {
    let response
    try {
      response = await axios.get(`http://localhost:3000/api/product/${id}`)
    } catch (error) {
      console.log('error', error)
    }
    return response.data
  }

export default {
  getProducts,
  getProductDetail,
}
