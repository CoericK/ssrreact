import "core-js";
import "regenerator-runtime/runtime";

import path from "path";

import * as React from "react";
import * as ReactDOM from "react-dom/server";

import { Provider } from "react-redux";
import { configureStore } from "./configureStore";
import { createMemoryHistory } from "history";
import rootSaga from "./redux/sagas";

import { StaticRouter } from "react-router-dom";

import App from "./App";

import express from "express";
import cors from "cors";

import { Helmet } from "react-helmet";

const NODE_ENV = process.env.NODE_ENV || "development";

console.log("NODE_ENV", NODE_ENV);

const app = express();
app.use(cors());

app.set("views", "./views");

app.set("view engine", "pug");

app.use("/assets", express.static("assets"));

if (NODE_ENV === "development") {
  const webpack = require("webpack");
  const WebpackHotMiddleware = require("webpack-hot-middleware");
  const WebpackDevMiddleware = require("webpack-dev-middleware");
  const config = require("../webpack.config.client.dev.babel");
  const compiler = webpack(config);
  app.use(
    WebpackDevMiddleware(compiler, {
      serverSideRender: true,
      publicPath: config.output.publicPath,
      stats: { colors: true }
    })
  );
  app.use(WebpackHotMiddleware(compiler));
}

const products = [
  { id: 1, name: "Product 1" },
  { id: 2, name: "Product 2" }
];

app.get("/api/products", (req, res) => {
  res.json({
    products
  });
});

app.get("/api/product/:id", (req, res) => {
  const product = products.filter(product => product.id === 1 * req.params.id);
  res.json(product.length === 1 ? product[0] : null);
});

app.get("/*", (req, res) => {
  const store = configureStore(
    createMemoryHistory({ initialEntries: [req.url] })
  );

  const context = {};

  const jsx = (
    <Provider store={store}>
      <StaticRouter location={req.url} context={context}>
        <App />
      </StaticRouter>
    </Provider>
  );

  store
    .runSaga(rootSaga)
    .toPromise()
    .then(() => {
      const html = ReactDOM.renderToString(jsx);
      const helmet = Helmet.renderStatic();

      if (context.status === 404) {
        res.status(404);
      }

      if (NODE_ENV === "production") {
        res.render("index.pug", {
          helmet,
          html,
          initialReduxState: JSON.stringify(store.getState())
        });
      } else {
        res.send(`
        <html>
        <head>
          ${helmet.title.toString()}
          ${helmet.meta.toString()}
        </head>
        <body>
        <div id="root">${html}</div>
        <script type="text/javascript">
          window.initialReduxState = JSON.parse('${JSON.stringify(
            store.getState()
          )}');
        </script>
        <script type="text/javascript" src="/assets/js/main.js"></script>
        </body>
        </html>
    `);
      }
    });

  ReactDOM.renderToString(jsx);

  store.close();
});

app.listen(3000, () => {
  console.log("Server running at port 3000");
});
