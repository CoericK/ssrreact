import { createStore, combineReducers, applyMiddleware } from "redux";
import createSagaMiddleware, { END } from "redux-saga";
import { composeWithDevTools } from "redux-devtools-extension";
import { connectRouter, routerMiddleware } from "connected-react-router";

import reducers from "./redux/reducers";
import rootSaga from "./redux/sagas";

export const configureStore = (history, preloadedState) => {
  const sagaMiddleware = createSagaMiddleware();
  const store = createStore(
    combineReducers({
      router: connectRouter(history),
      ...reducers
    }),
    preloadedState,
    composeWithDevTools(
      applyMiddleware(sagaMiddleware, routerMiddleware(history))
    )
  );

  store.runSaga = sagaMiddleware.run;
  store.close = () => store.dispatch(END);
  //sagaMiddleware.run(rootSaga)
  return store;
};
