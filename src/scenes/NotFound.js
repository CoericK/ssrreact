import * as React from "react";
import { Helmet } from "react-helmet";

const NotFound = ({ staticContext }) => {
  if (staticContext) {
    staticContext.status = 404;
  }

  return (
    <>
      <Helmet>
        <title>Page Not Found - My Web!</title>
      </Helmet>
      <h1>Oops, nothing here!</h1>
    </>
  );
};

export default NotFound;
