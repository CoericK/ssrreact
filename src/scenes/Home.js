import * as React from "react";
import { connect } from "react-redux";
import { productsActions } from "../redux/actions";

import { Route, Switch, NavLink } from "react-router-dom";

class Home extends React.Component {
  constructor(props) {
    super(props);
    if (props.products && props.products.length === 0) {
      props.fetchProducts();
    }
  }
  render() {
    const { products } = this.props;
    return (
      <div>
        <h1>Product List</h1>
        {products && products.length > 0 && (
          <ul>
            {products.map(product => {
              const { id, name } = product;
              return (
                <li key={product.id}>
                  <NavLink to={`/product/${product.id}`}>
                    {product.name}
                  </NavLink>
                </li>
              );
            })}
          </ul>
        )}
      </div>
    );
  }
}
const mapStateToProps = state => {
  return {
    products: state.Products.products
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchProducts: () => dispatch(productsActions.fetchProducts())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
