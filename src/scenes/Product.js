import * as React from "react";
import { connect } from "react-redux";
import { productsActions } from "../redux/actions";
import { NavLink } from "react-router-dom";
import { Helmet } from "react-helmet";
class Product extends React.Component {
  constructor(props) {
    super(props);
    console.log('Product constructor props', props)
    if (props.product === null || props.product.id !== 1* props.match.params.id) {
      console.log('should fetch product')
      props.fetchProductDetail(props.match.params.id);
    }
  }

  render() {

    const { product, match } = this.props;
    if (!product || (product && product.id !== 1 * match.params.id)) {
      return <div>Loading...</div>;
    }

    return (
      <div>
        <Helmet>
          <title>{product.name} - My Web!</title>
          <meta name="description" content={`${product.name}`} />
        </Helmet>
        <span>
          <NavLink to={`/`}>Home</NavLink>
        </span>
        <h1>Product Detail:</h1>
        {product && (
          <>
            <span>Product ID: {product.id}</span>
            <br />
            <span>Product Name: {product.name}</span>
          </>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    product: state.Products.product || null
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchProductDetail: id => dispatch(productsActions.fetchProductDetail(id))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Product);
