import * as React from "react";
import * as ReactDOM from "react-dom";
import { Provider } from "react-redux";
import App from "./App";
import { configureStore } from "./configureStore";
import { BrowserRouter } from "react-router-dom";
import rootSaga from "./redux/sagas";

import { ConnectedRouter } from "connected-react-router";

import { createBrowserHistory } from "history";

const history = createBrowserHistory();

const store = configureStore(history, window.initialReduxState);
store.runSaga(rootSaga);
ReactDOM.hydrate(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <App />
    </ConnectedRouter>
  </Provider>,
  document.getElementById("root")
);
