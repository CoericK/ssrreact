import { all, call, put, takeLatest } from "redux-saga/effects";
import { productsActionTypes as TYPES } from "../actionTypes";
import API from "../../api";

function* fetchProducts(action) {
  let result;
  try {
    result = yield call(API.getProducts);
  } catch (error) {
    console.log("saga fetchProducts error", error);
  }

  yield put({ type: TYPES.FETCH_PRODUCTS_SUCCESS, payload: result });
}

function* fetchProductDetail(action) {
  let result;
  try {
    result = yield call(API.getProductDetail, action.payload.id);
  } catch (error) {
    console.log("saga fetchProducts error", error);
  }

  yield put({ type: TYPES.FETCH_PRODUCT_DETAIL_SUCCESS, payload: result });
}

function* watchFetchProducts() {
  yield takeLatest(TYPES.FETCH_PRODUCTS_REQUEST, fetchProducts);
}

function* watchFetchProduct() {
  yield takeLatest(TYPES.FETCH_PRODUCT_DETAIL_REQUEST, fetchProductDetail);
}

const rootSaga = function*() {
  yield all([call(watchFetchProducts), call(watchFetchProduct)]);
  /*
  yield all([
    call(yield takeLatest(TYPES.FETCH_PRODUCTS_REQUEST, fetchProducts)),
    call()
  ]);*/
};

export default rootSaga;
