import { productsActionTypes as TYPES } from "../actionTypes";
const initialState = {
  product: null,
  products: []
};
const products = (state = initialState, action) => {
  switch (action.type) {
    case TYPES.FETCH_PRODUCTS_SUCCESS:
      return {
        ...state,
        products: action.payload.products
      };

    case TYPES.FETCH_PRODUCT_DETAIL_SUCCESS:
      return {
        ...state,
        product: action.payload
      };
    default:
      return state;
  }
};

export default products;
