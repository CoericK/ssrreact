import { productsActionTypes as TYPES } from "../actionTypes";

const fetchProducts = () => ({
  type: TYPES.FETCH_PRODUCTS_REQUEST
});

const fetchProductDetail = id => ({
  type: TYPES.FETCH_PRODUCT_DETAIL_REQUEST,
  payload: {
    id
  }
});

export const productsActions = {
  fetchProducts,
  fetchProductDetail
};
