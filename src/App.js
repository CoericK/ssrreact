import { hot } from "react-hot-loader/root";
import * as React from "react";
import { Helmet } from "react-helmet";

import { Route, Switch, NavLink } from "react-router-dom";

import Home from "./scenes/Home";
import Product from "./scenes/Product";
import NotFound from "./scenes/NotFound";

const App = ()=>{
  return (
    <>
      <Helmet>
        <title>My Web!</title>
        <meta name="description" content="My Awesome Website" />
      </Helmet>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/product/:id" component={Product} />
        <Route component={NotFound} />
      </Switch>
    </>
  );
}
export default hot(App);
