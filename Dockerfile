FROM node:13.2.0-alpine AS build

RUN mkdir -p /root/nodejs

WORKDIR /root/nodejs

COPY . .

RUN yarn install && yarn build

FROM node:13.2.0-alpine

COPY --from=build /root/nodejs/dist /root/nodejs

WORKDIR /root/nodejs

RUN yarn install --production && yarn cache clean

ENV NODE_ENV=production

RUN ls -latr

ENTRYPOINT [ "node", "server.js" ]